﻿using NETProjectTutorial.dao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NETProjectTutorial.entities;
using System.IO;

namespace NETProjectTutorial.implements
{
    class DaoImplementsDetalleFactura : IDaoDetalleFactura
    {
        //header DetalleFactura
        private BinaryReader brhDetalleFactura;
        private BinaryWriter bwhDetalleFactura;
        //data DetalleFactura
        private BinaryReader brdDetalleFactura;
        private BinaryWriter bwdDetalleFactura;

        private FileStream fshDetalleFactura;
        private FileStream fsdDetalleFactura;

        private const string FILENAME_HEADER = "hDetalleFactura.dat";
        private const string FILENAME_DATA = "dDetalleFactura.dat";
        private const int SIZE = 390;

        public DaoImplementsDetalleFactura() { }

        private void open()
        {
            try
            {
                fsdDetalleFactura = new FileStream(FILENAME_DATA,
                    FileMode.OpenOrCreate, FileAccess.ReadWrite);
                if (!File.Exists(FILENAME_HEADER))
                {
                    fshDetalleFactura = new FileStream(FILENAME_HEADER,
                        FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    brhDetalleFactura = new BinaryReader(fshDetalleFactura);
                    bwhDetalleFactura = new BinaryWriter(fshDetalleFactura);

                    brdDetalleFactura = new BinaryReader(fsdDetalleFactura);
                    bwdDetalleFactura = new BinaryWriter(fsdDetalleFactura);

                    bwhDetalleFactura.BaseStream.Seek(0, SeekOrigin.Begin);
                    bwhDetalleFactura.Write(0);//n
                    bwhDetalleFactura.Write(0);//k
                }
                else
                {
                    fshDetalleFactura = new FileStream(FILENAME_HEADER,
                        FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    brhDetalleFactura = new BinaryReader(fshDetalleFactura);
                    bwhDetalleFactura = new BinaryWriter(fshDetalleFactura);
                    brdDetalleFactura = new BinaryReader(fsdDetalleFactura);
                    bwdDetalleFactura = new BinaryWriter(fsdDetalleFactura);
                }
            }
            catch (IOException e)
            {
                throw new IOException(e.Message);
            }
        }

        private void close()
        {
            try
            {
                if (brdDetalleFactura != null)
                {
                    brdDetalleFactura.Close();
                }
                if (brhDetalleFactura != null)
                {
                    brhDetalleFactura.Close();
                }
                if (bwdDetalleFactura != null)
                {
                    bwdDetalleFactura.Close();
                }
                if (bwhDetalleFactura != null)
                {
                    bwhDetalleFactura.Close();
                }
                if (fsdDetalleFactura != null)
                {
                    fsdDetalleFactura.Close();
                }
                if (fshDetalleFactura != null)
                {
                    fshDetalleFactura.Close();
                }
            }
            catch (IOException e)
            {
                throw new IOException(e.Message);
            }
        }

       

        public bool delete(DetalleFactura t)
        {
            throw new NotImplementedException();
        }

        public List<DetalleFactura> findAll()
        {
            //open();
            //List<DetalleFactura> productos = new List<DetalleFactura>();

            //brhDetalleFactura.BaseStream.Seek(0, SeekOrigin.Begin);
            //int n = brhDetalleFactura.ReadInt32();
            //for (int i = 0; i < n; i++)
            //{
            //    //calculamos posicion cabecera
            //    long hpos = 8 + i * 4;
            //    brhDetalleFactura.BaseStream.Seek(hpos, SeekOrigin.Begin);
            //    int index = brhDetalleFactura.ReadInt32();
            //    //calculamos posicion de los datos
            //    long dpos = (index - 1) * SIZE;
            //    brdDetalleFactura.BaseStream.Seek(dpos, SeekOrigin.Begin);

            //    int id = brdDetalleFactura.ReadInt32();
            //    string factura = brdDetalleFactura.ReadString();
            //    string producto = brdDetalleFactura.ReadString();
            //    int cantidad = brdDetalleFactura.ReadInt32();
            //    double precio = brdDetalleFactura.ReadDouble();
            //    DetalleFactura p = new DetalleFactura(id, (Factura.Factura)Enum.Parse(typeof(DetalleFactura.Factura), factura), Producto producto, cantidad, precio);
            //    productos.Add(p);
            //}

            //close();
            //return productos;
            throw new NotImplementedException();
        }

        public DetalleFactura findByFactura(string factura)
        {
            throw new NotImplementedException();
        }

        public DetalleFactura findById(int id)
        {
            throw new NotImplementedException();
        }

        public List<DetalleFactura> findByProducto(string producto)
        {
            throw new NotImplementedException();
        }

        public void save(DetalleFactura t)
        {
            open();
            brhDetalleFactura.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brhDetalleFactura.ReadInt32();
            int k = brhDetalleFactura.ReadInt32();

            long dpos = k * SIZE;
            bwdDetalleFactura.BaseStream.Seek(dpos, SeekOrigin.Begin);

            bwdDetalleFactura.Write(++k);
            bwdDetalleFactura.Write(t.Factura.ToString());
            bwdDetalleFactura.Write(t.Producto.ToString());
            bwdDetalleFactura.Write(t.Cantidad);
            bwdDetalleFactura.Write(t.Precio);


            bwhDetalleFactura.BaseStream.Seek(0, SeekOrigin.Begin);
            bwhDetalleFactura.Write(++n);
            bwhDetalleFactura.Write(k);

            long hpos = 8 + (n - 1) * 4;
            bwhDetalleFactura.BaseStream.Seek(hpos, SeekOrigin.Begin);
            bwhDetalleFactura.Write(k);
            close();
        }

        public int update(DetalleFactura t)
        {
            open();
            brhDetalleFactura.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brhDetalleFactura.ReadInt32();
            int k = brhDetalleFactura.ReadInt32();

            int cpos;
            for (cpos = 0; cpos < n; cpos++)
            {
                int id = brhDetalleFactura.ReadInt32();
                if (id == t.Id)
                {
                    long dpos = cpos * SIZE;
                    bwdDetalleFactura.Write(t.Id);
                    bwdDetalleFactura.Write(t.Factura.ToString());
                    bwdDetalleFactura.Write(t.Producto.ToString());
                    bwdDetalleFactura.Write(t.Cantidad);
                    bwdDetalleFactura.Write(t.Precio);
                   

                    close();
                    return t.Id;

                }
            }
            return -1;
        }
    }
}
