﻿using NETProjectTutorial.entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.implements
{
    class DaoImplementsFactura : IDaoFactura
    {
        //header Factura
        private BinaryReader brhFactura;
        private BinaryWriter bwhFactura;
        //data Factura
        private BinaryReader brdFactura;
        private BinaryWriter bwdFactura;

        private FileStream fshFactura;
        private FileStream fsdFactura;

        private const string FILENAME_HEADER = "hFactura.dat";
        private const string FILENAME_DATA = "dFactura.dat";
        private const int SIZE = 390;

        public DaoImplementsFactura()
        {

        }
        private void open()
        {
            try
            {
                fsdFactura = new FileStream(FILENAME_DATA,
                    FileMode.OpenOrCreate, FileAccess.ReadWrite);
                if (!File.Exists(FILENAME_HEADER))
                {
                    fshFactura = new FileStream(FILENAME_HEADER,
                        FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    brhFactura = new BinaryReader(fshFactura);
                    bwhFactura = new BinaryWriter(fshFactura);

                    brdFactura = new BinaryReader(fsdFactura);
                    bwdFactura = new BinaryWriter(fsdFactura);

                    bwhFactura.BaseStream.Seek(0, SeekOrigin.Begin);
                    bwhFactura.Write(0);//n
                    bwhFactura.Write(0);//k
                }
                else
                {
                    fshFactura = new FileStream(FILENAME_HEADER,
                        FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    brhFactura = new BinaryReader(fshFactura);
                    bwhFactura = new BinaryWriter(fshFactura);
                    brdFactura = new BinaryReader(fsdFactura);
                    bwdFactura = new BinaryWriter(fsdFactura);
                }
            }
            catch (IOException e)
            {
                throw new IOException(e.Message);
            }
        }

        private void close()
        {
            try
            {
                if (brdFactura != null)
                {
                    brdFactura.Close();
                }
                if (brhFactura != null)
                {
                    brhFactura.Close();
                }
                if (bwdFactura != null)
                {
                    bwdFactura.Close();
                }
                if (bwhFactura != null)
                {
                    bwhFactura.Close();
                }
                if (fsdFactura != null)
                {
                    fsdFactura.Close();
                }
                if (fshFactura != null)
                {
                    fshFactura.Close();
                }
            }
            catch (IOException e)
            {
                throw new IOException(e.Message);
            }
        }

        public List<Factura> findAll()
        {
            open();
            List<Factura> facturas = new List<Factura>();

            brhFactura.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brhFactura.ReadInt32();
            for (int i = 0; i < n; i++)
            {
                //calculamos posicion cabecera
                long hpos = 8 + i * 4;
                brhFactura.BaseStream.Seek(hpos, SeekOrigin.Begin);
                int index = brhFactura.ReadInt32();
                //calculamos posicion de los dato
                long dpos = (index - 1) * SIZE;
                brdFactura.BaseStream.Seek(dpos, SeekOrigin.Begin);

                int id = brdFactura.ReadInt32();
                string cod_factura = brdFactura.ReadString();
                DateTime fecha = DateTime.Parse(brdFactura.ReadString());
                int empleadoId = brdFactura.ReadInt32();
                Empleado empleado = new DaoImplementsEmpleado().findById(empleadoId);
                int clineteId = brdFactura.ReadInt32();
                Cliente cliente = new DaoImplementsCliente().findById(clineteId);
                string observaciones = brdFactura.ReadString();
                double subtotal = brdFactura.ReadDouble();
                double iva = brdFactura.ReadDouble();
                double total = brdFactura.ReadDouble();
                Factura f = new Factura(id, cod_factura, fecha, empleado, cliente, observaciones, subtotal, iva, total);
                facturas.Add(f);
            }

            close();
            return facturas;
        }

        public void save(Factura t)
        {
            open();
            brhFactura.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brhFactura.ReadInt32();
            int k = brhFactura.ReadInt32();

            long dpos = k * SIZE;

            bwdFactura.BaseStream.Seek(dpos, SeekOrigin.Begin);

            bwdFactura.Write(++k);
            bwdFactura.Write(t.Cod_factura);
            bwdFactura.Write(t.Fecha.ToString());
            bwdFactura.Write(t.Empleado.ToString());
            bwdFactura.Write(t.Cliente.ToString());
            bwdFactura.Write(t.Observaciones);
            bwdFactura.Write(t.Subtotal);
            bwdFactura.Write(t.Iva);
            bwdFactura.Write(t.Total);

            bwhFactura.BaseStream.Seek(0, SeekOrigin.Begin);
            bwhFactura.Write(++n);
            bwhFactura.Write(k);

            long hpos = 8 + (n - 1) * 4;
            bwhFactura.BaseStream.Seek(hpos, SeekOrigin.Begin);
            bwhFactura.Write(k);
            close();
            
        }

        public Factura findById(int id)
        {
            Factura f = null;
            open();
            brhFactura.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brhFactura.ReadInt32();
            int k = brhFactura.ReadInt32();

            int fpos;
            for (fpos = 0; fpos < n; fpos++)
            {
                int fid = brhFactura.ReadInt32();
                if (fid == id)
                {
                    long dpos = fpos * SIZE;
                    brdFactura.BaseStream.Seek(dpos, SeekOrigin.Begin);

                    brdFactura.ReadInt32();//lee el id
                    string cod_factura = brdFactura.ReadString();
                    DateTime fecha = DateTime.Parse(brdFactura.ReadString());
                    int empleadoId = brdFactura.ReadInt32();
                    Empleado empleado = new DaoImplementsEmpleado().findById(empleadoId);
                    int clineteId = brdFactura.ReadInt32();
                    Cliente cliente = new DaoImplementsCliente().findById(clineteId);
                    string observaciones = brdFactura.ReadString();
                    double subtotal = brdFactura.ReadDouble();
                    double iva = brdFactura.ReadDouble();
                    double total = brdFactura.ReadDouble();
                    f = new Factura(id, cod_factura, fecha, empleado, cliente, observaciones, subtotal, iva, total);

                    break;
                }
            }
            close();

            return f;
        }
    }
    
}
