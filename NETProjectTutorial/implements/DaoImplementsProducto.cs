﻿using NETProjectTutorial.dao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NETProjectTutorial.entities;
using System.IO;

namespace NETProjectTutorial.implements
{
    class DaoImplementsProducto : IDaoProducto
    {
        //header Producto
        private BinaryReader brhProducto;
        private BinaryWriter bwhProducto;
        //data producto
        private BinaryReader brdProducto;
        private BinaryWriter bwdProducto;

        private FileStream fshProducto;
        private FileStream fsdProducto;

        private const string FILENAME_HEADER = "hproducto.dat";
        private const string FILENAME_DATA = "dproducto.dat";
        private const int SIZE = 390;

        public DaoImplementsProducto() { }

        private void open()
        {
            try
            {
                fsdProducto = new FileStream(FILENAME_DATA,
                    FileMode.OpenOrCreate, FileAccess.ReadWrite);
                if (!File.Exists(FILENAME_HEADER))
                {
                    fshProducto = new FileStream(FILENAME_HEADER,
                        FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    brhProducto = new BinaryReader(fshProducto);
                    bwhProducto = new BinaryWriter(fshProducto);

                    brdProducto = new BinaryReader(fsdProducto);
                    bwdProducto = new BinaryWriter(fsdProducto);

                    bwhProducto.BaseStream.Seek(0, SeekOrigin.Begin);
                    bwhProducto.Write(0);//n
                    bwhProducto.Write(0);//k
                }
                else
                {
                    fshProducto = new FileStream(FILENAME_HEADER,
                        FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    brhProducto = new BinaryReader(fshProducto);
                    bwhProducto = new BinaryWriter(fshProducto);
                    brdProducto = new BinaryReader(fsdProducto);
                    bwdProducto = new BinaryWriter(fsdProducto);
                }
            }
            catch (IOException e)
            {
                throw new IOException(e.Message);
            }
        }

        private void close()
        {
            try
            {
                if (brdProducto != null)
                {
                    brdProducto.Close();
                }
                if (brhProducto != null)
                {
                    brhProducto.Close();
                }
                if (bwdProducto != null)
                {
                    bwdProducto.Close();
                }
                if (bwhProducto != null)
                {
                    bwhProducto.Close();
                }
                if (fsdProducto != null)
                {
                    fsdProducto.Close();
                }
                if (fshProducto != null)
                {
                    fshProducto.Close();
                }
            }
            catch (IOException e)
            {
                throw new IOException(e.Message);
            }
        }
        public bool delete(Producto t)
        {
            throw new NotImplementedException();
        }

        public List<Producto> findAll()
        {
            open();
            List<Producto> productos = new List<Producto>();

            brhProducto.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brhProducto.ReadInt32();
            for (int i = 0; i < n; i++)
            {
                //calculamos posicion cabecera
                long hpos = 8 + i * 4;
                brhProducto.BaseStream.Seek(hpos, SeekOrigin.Begin);
                int index = brhProducto.ReadInt32();
                //calculamos posicion de los datos
                long dpos = (index - 1) * SIZE;
                brdProducto.BaseStream.Seek(dpos, SeekOrigin.Begin);

                int id = brdProducto.ReadInt32();
               string sku = brdProducto.ReadString();
               string nombre = brdProducto.ReadString();
               string descripcion = brdProducto.ReadString();
               int cantidad = brdProducto.ReadInt32();
               double precio = brdProducto.ReadDouble();
                Producto p = new Producto(id, sku, nombre,
                    descripcion, cantidad, precio);
                productos.Add(p);
            }

            close();
            return productos;
        }

       

        public List<Producto> findByNombre(string nombre)
        {
            throw new NotImplementedException();
        }

        public Producto findBySKU(string sku)
        {
            throw new NotImplementedException();
        }

        public void save(Producto t)
        {
            open();
            brhProducto.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brhProducto.ReadInt32();
            int k = brhProducto.ReadInt32();

            long dpos = k * SIZE;
            bwdProducto.BaseStream.Seek(dpos, SeekOrigin.Begin);

            bwdProducto.Write(++k);
            bwdProducto.Write(t.Sku);
            bwdProducto.Write(t.Nombre);
            bwdProducto.Write(t.Descripcion);
            bwdProducto.Write(t.Cantidad);
            bwdProducto.Write(t.Precio);


            bwhProducto.BaseStream.Seek(0, SeekOrigin.Begin);
            bwhProducto.Write(++n);
            bwhProducto.Write(k);

            long hpos = 8 + (n - 1) * 4;
            bwhProducto.BaseStream.Seek(hpos, SeekOrigin.Begin);
            bwhProducto.Write(k);
            close();
        }

        public int update(Producto t)
        {
            open();
            brhProducto.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brhProducto.ReadInt32();
            int k = brhProducto.ReadInt32();

            int cpos;
            for (cpos = 0; cpos < n; cpos++)
            {
                int id = brhProducto.ReadInt32();
                if (id == t.Id)
                {
                    long dpos = cpos * SIZE;
                    bwdProducto.Write(t.Id);
                    bwdProducto.Write(t.Sku);
                    bwdProducto.Write(t.Nombre);
                    bwdProducto.Write(t.Descripcion);
                    bwdProducto.Write(t.Cantidad);
                    bwdProducto.Write(t.Precio);

                    close();
                    return t.Id;

                }
            }
            return -1;
        }

       
        public List<Producto> findByProducto(string producto)
        {
            throw new NotImplementedException();
        }

       public Producto findById(int id)
        {
            Producto productos = null;
            open();
            brhProducto.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brhProducto.ReadInt32();
            int k = brhProducto.ReadInt32();

            int ppos;
            for (ppos = 0; ppos < n; ppos++)
            {
                int pid = brhProducto.ReadInt32();
                if (pid == id)
                {
                    long dpos = ppos * SIZE;
                    brdProducto.BaseStream.Seek(dpos, SeekOrigin.Begin);

                    brdProducto.ReadInt32();//lee el id
                    string sku = brdProducto.ReadString();
                    string nombre = brdProducto.ReadString();
                    string descripcion = brdProducto.ReadString();
                    int cantidad = brdProducto.ReadInt32();
                    double precio = brdProducto.ReadDouble();
                    productos = new Producto(id, sku, nombre,
                        descripcion, cantidad, precio);

                    break;
                }
            }
            close();

            return productos;
        }

        public Producto findByFactura(string producto)
        {
            throw new NotImplementedException();
        }
    }
}
