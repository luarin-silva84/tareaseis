﻿using NETProjectTutorial.dao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NETProjectTutorial.entities;
using System.IO;

namespace NETProjectTutorial.implements
{
    class DaoImplementsEmpleado : IDaoEmpleado
    {
        //header Empleado
        private BinaryReader brhEmpleado;
        private BinaryWriter bwhEmpleado;
        //data Empleado
        private BinaryReader brdEmpleado;
        private BinaryWriter bwdEmpleado;

        private FileStream fshEmpleado;
        private FileStream fsdEmpleado;

        private const string FILENAME_HEADER = "hEmpleado.dat";
        private const string FILENAME_DATA = "dCliente.dat";
        private const int SIZE = 390;

        public DaoImplementsEmpleado() { }

        private void open()
        {
            try
            {
                fsdEmpleado = new FileStream(FILENAME_DATA,
                    FileMode.OpenOrCreate, FileAccess.ReadWrite);
                if (!File.Exists(FILENAME_HEADER))
                {
                    fshEmpleado = new FileStream(FILENAME_HEADER,
                        FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    brhEmpleado = new BinaryReader(fshEmpleado);
                    bwhEmpleado = new BinaryWriter(fshEmpleado);

                    brdEmpleado = new BinaryReader(fsdEmpleado);
                    bwdEmpleado = new BinaryWriter(fsdEmpleado);

                    bwhEmpleado.BaseStream.Seek(0, SeekOrigin.Begin);
                    bwhEmpleado.Write(0);//n
                    bwhEmpleado.Write(0);//k
                }
                else
                {
                    fshEmpleado = new FileStream(FILENAME_HEADER,
                        FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    brhEmpleado = new BinaryReader(fshEmpleado);
                    bwhEmpleado = new BinaryWriter(fshEmpleado);
                    brdEmpleado = new BinaryReader(fsdEmpleado);
                    bwdEmpleado = new BinaryWriter(fsdEmpleado);
                }
            }
            catch (IOException e)
            {
                throw new IOException(e.Message);
            }
        }

        private void close()
        {
            try
            {
                if (brdEmpleado != null)
                {
                    brdEmpleado.Close();
                }
                if (brhEmpleado != null)
                {
                    brhEmpleado.Close();
                }
                if (bwdEmpleado != null)
                {
                    bwdEmpleado.Close();
                }
                if (bwhEmpleado != null)
                {
                    bwhEmpleado.Close();
                }
                if (fsdEmpleado != null)
                {
                    fsdEmpleado.Close();
                }
                if (fshEmpleado != null)
                {
                    fshEmpleado.Close();
                }
            }
            catch (IOException e)
            {
                throw new IOException(e.Message);
            }
        }


        public bool delete(Empleado t)
        {
            throw new NotImplementedException();
        }

        public List<Empleado> findAll()
        {
            open();
            List<Empleado> empleados = new List<Empleado>();

            brhEmpleado.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brhEmpleado.ReadInt32();
            for (int i = 0; i < n; i++)
            {
                //calculamos posicion cabecera
                long hpos = 8 + i * 4;
                brhEmpleado.BaseStream.Seek(hpos, SeekOrigin.Begin);
                int index = brhEmpleado.ReadInt32();
                //calculamos posicion de los dato
                long dpos = (index - 1) * SIZE;
                brdEmpleado.BaseStream.Seek(dpos, SeekOrigin.Begin);

                int id = brdEmpleado.ReadInt32();
                string inss = brdEmpleado.ReadString();
                double salario = brdEmpleado.ReadDouble();
                string cedula = brdEmpleado.ReadString();
                string nombres = brdEmpleado.ReadString();
                string apellidos = brdEmpleado.ReadString();
                string tconvencional = brdEmpleado.ReadString();
                string direccion = brdEmpleado.ReadString();
                string tcelular = brdEmpleado.ReadString();
                string sexo = brdEmpleado.ReadString();
                Empleado c = new Empleado(id, nombres, apellidos,
                    cedula, inss, direccion, salario,tconvencional,tcelular,(Empleado.SEXO)Enum.Parse(typeof(Empleado.SEXO),sexo));
                empleados.Add(c);
            }

            close();
            return empleados;
        }

        public Empleado findByCedula(string cedula)
        {
            throw new NotImplementedException();
        }

        public Empleado findById(int id)
        {
            throw new NotImplementedException();
        }

        public List<Empleado> findByLastname(string lastname)
        {
            throw new NotImplementedException();
        }

        public void save(Empleado t)
        {
            open();
            brhEmpleado.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brhEmpleado.ReadInt32();
            int k = brhEmpleado.ReadInt32();

            long dpos = k * SIZE;
            bwdEmpleado.BaseStream.Seek(dpos, SeekOrigin.Begin);

            bwdEmpleado.Write(++k);
            bwdEmpleado.Write(t.Inss);
            bwdEmpleado.Write(t.Salario);
            bwdEmpleado.Write(t.Cedula);
            bwdEmpleado.Write(t.Nombre);
            bwdEmpleado.Write(t.Apellidos);
            bwdEmpleado.Write(t.Tconvencional);
            bwdEmpleado.Write(t.Tcelular);
            bwdEmpleado.Write(t.Sexo.ToString());

            bwhEmpleado.BaseStream.Seek(0, SeekOrigin.Begin);
            bwhEmpleado.Write(++n);
            bwhEmpleado.Write(k);

            long hpos = 8 + (n - 1) * 4;
            bwhEmpleado.BaseStream.Seek(hpos, SeekOrigin.Begin);
            bwhEmpleado.Write(k);
            close();
        }

        public int update(Empleado t)
        {
            open();
            brhEmpleado.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brhEmpleado.ReadInt32();
            int k = brhEmpleado.ReadInt32();

            int cpos;
            for (cpos = 0; cpos < n; cpos++)
            {
                int id = brhEmpleado.ReadInt32();
                if (id == t.Id)
                {
                    long dpos = cpos * SIZE;
                    bwdEmpleado.Write(t.Id);
                    bwdEmpleado.Write(t.Inss);
                    bwdEmpleado.Write(t.Salario);
                    bwdEmpleado.Write(t.Cedula);
                    bwdEmpleado.Write(t.Nombre);
                    bwdEmpleado.Write(t.Apellidos);
                    bwdEmpleado.Write(t.Tconvencional);
                    bwdEmpleado.Write(t.Tcelular);
                    bwdEmpleado.Write(t.Sexo.ToString());
                    close();
                    return t.Id;

                }
            }
            return -1;
        }
    }
}
