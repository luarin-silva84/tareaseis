﻿using NETProjectTutorial.entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.dao
{
    interface IDaoDetalleFactura : IDao<DetalleFactura>
    {
        DetalleFactura findById(int id);
        DetalleFactura findByFactura(string factura);
        List<DetalleFactura> findByProducto(string producto);
    }
}
