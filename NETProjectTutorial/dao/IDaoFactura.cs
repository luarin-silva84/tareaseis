﻿using NETProjectTutorial.entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.dao
{
    interface IDaofactura : IDao<Factura>
    {
        Factura findById(int id);
    }
}
