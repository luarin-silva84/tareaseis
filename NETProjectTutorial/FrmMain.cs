﻿using NETProjectTutorial.entities;
using NETProjectTutorial.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmMain : Form
    {
        private DataTable dtProductos;
        private ProductoModel productoModel;
        private EmpleadoModel empleadoModel;
        private ClienteModel clienteModel;

        public FrmMain()
        {
            InitializeComponent();
            productoModel = new ProductoModel();
            empleadoModel = new EmpleadoModel();
            clienteModel = new ClienteModel();
        }

        private void productosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionProductos fgp = new FrmGestionProductos();
            fgp.MdiParent = this;
            fgp.DsProductos = dsProductos;
            fgp.Show();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            dtProductos = dsProductos.Tables["Producto"];

            //ProductoModel.Populate();
            List<Producto> productos = productoModel.GetProductos();
            if (productos.Count == 0)
            {
                productoModel.Populate();
                productos = productoModel.GetProductos();
            }
            foreach (Producto p in productos)
            {
                Console.WriteLine(p.Id);
                DataRow drProducto = dsProductos.Tables["Producto"].NewRow();
                drProducto["kId"] = p.Id;
                drProducto["Sku"] = p.Sku;
                drProducto["Nombre"] = p.Nombre;
                drProducto["Descripcion"] = p.Descripcion;
                drProducto["Cantidad"] = p.Cantidad;
                drProducto["Precio"] = p.Precio;
                drProducto["SKUN"] = p.Sku + " " + p.Nombre;

                dsProductos.Tables["Producto"].Rows.Add(drProducto);
                drProducto.AcceptChanges();

            }

            //EmpleadoModel.Populate();
            List<Empleado> empleados = empleadoModel.GetListEmpleado();
            if(empleados.Count == 0)
            {
                empleadoModel.Populate();
                empleados = empleadoModel.GetListEmpleado();
            }
            foreach (Empleado emp in empleados)
            {
                DataRow drEmpleado = dsProductos.Tables["Empleado"].NewRow();
                drEmpleado["Id"] = emp.Id;
                drEmpleado["Inss"] = emp.Inss;
                drEmpleado["Salario"] = emp.Salario;
                drEmpleado["Cedula"] = emp.Cedula;
                drEmpleado["Nombres"] = emp.Nombre;
                drEmpleado["Apellidos"] = emp.Apellidos;
                drEmpleado["Telefono"] = emp.Tconvencional;
                drEmpleado["Direccion"] = emp.Direccion;
                drEmpleado["Celular"] = emp.Tcelular;
                drEmpleado["Sexo"] = emp.Sexo;
                drEmpleado["NA"] = emp.Nombre + " " + emp.Apellidos;

                dsProductos.Tables["Emplado"].Rows.Add(drEmpleado);
                drEmpleado.AcceptChanges();
            }

            //new ClienteModel().Populate();
            List<Cliente> clientes = clienteModel.GetListCliente();
            if (clientes.Count == 0)
            {
                clienteModel.Populate();
                clientes = clienteModel.GetListCliente();
            }
            foreach (Cliente client in clientes)
            {
                DataRow drCliente = dsProductos.Tables["Cliente"].NewRow();
                drCliente["Id"] = client.Id;
                drCliente["Cedula"] = client.Cedula;
                drCliente["Nombres"] = client.Nombres;
                drCliente["Apellidos"] = client.Apellidos;
                drCliente["Telefono"] = client.Telefono;
                drCliente["Correo"] = client.Correo;
                drCliente["Direccion"] = client.Direccion;
                drCliente["NA"] = client.Nombres + " " + client.Apellidos;
                
                dsProductos.Tables["Cliente"].Rows.Add(drCliente);
                drCliente.AcceptChanges();
            }

        }

        private void EmpleadosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionEmpleados fge = new FrmGestionEmpleados();
            fge.MdiParent = this;
            fge.DsEmpleados = dsProductos;
            fge.Show();
        }

        private void NuevaFacturaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmFactura ff = new FrmFactura();
            ff.MdiParent = this;
            ff.DsSistema = dsProductos;
            ff.Show();
        }

        private void FacturasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionFacturas fgf = new FrmGestionFacturas();
            fgf.MdiParent = this;
            fgf.DsFacturas = dsProductos;
            fgf.Show();
        }

        private void ClientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionClientes fgc = new FrmGestionClientes();
            fgc.MdiParent = this;
            fgc.DsClientes = dsProductos;
            fgc.Show();
        }
    }
}
