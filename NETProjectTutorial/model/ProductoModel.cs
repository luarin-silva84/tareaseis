﻿using NETProjectTutorial.entities;
using NETProjectTutorial.implements;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.model
{
    class ProductoModel
    {
        private static  List<Producto> listproductos = new List<Producto>();
        private implements.DaoImplementsProducto daoproducto;

        public ProductoModel()
        {
            daoproducto = new DaoImplementsProducto();
        }
        public  List<Producto> GetProductos()
        {
            return daoproducto.findAll();
        }

        public void Populate()
        {
            /*Producto[] pdts =
            {
                new Producto(1,"Milk2514","Leche entera","Leche enterea 3% grasa",25,30.0),
                new Producto(2,"Milk2515","Leche descremada","Leche descremada 0% grasa",25,40.0),
                new Producto(3,"Milk2516","Leche deslactosada","Leche deslactosada 2% grasa",25,35.0)
            };*/

            listproductos = JsonConvert.DeserializeObject<List<Producto>>(System.Text.Encoding.Default.GetString(NETProjectTutorial.Properties.Resources.Producto_data));
            foreach (Producto e in listproductos)
            {
                daoproducto.save(e);
            }
            
        }

        public void save(DataRow producto)
        {
            Producto e = new Producto();
            e.Id = Convert.ToInt32(producto["Id"].ToString());
            e.Sku = producto["Sku"].ToString();
            e.Nombre = producto["Nombre"].ToString();
            e.Descripcion = producto["Descripcion"].ToString();
            e.Cantidad = Convert.ToInt32(producto["Cantidad"].ToString());
            e.Precio = Convert.ToDouble(producto["Precio"].ToString());
            daoproducto.save(e);

        }

        public void update(DataRow producto)
        {
            Producto e = new Producto();
            e.Id = Convert.ToInt32(producto["Id"].ToString());
            e.Sku = producto["Sku"].ToString();
            e.Nombre = producto["Nombre"].ToString();
            e.Descripcion = producto["Descripcion"].ToString();
            e.Cantidad = Convert.ToInt32(producto["Cantidad"].ToString());
            e.Precio = Convert.ToDouble(producto["Precio"].ToString());
            daoproducto.update(e);
        }

        public Producto findById(int id)
        {
            return daoproducto.findById(id);
        }
    }
}
