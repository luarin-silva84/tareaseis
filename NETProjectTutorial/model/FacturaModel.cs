﻿using NETProjectTutorial.entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.model
{
    class FacturaModel
    {
        private implements.DaoImplementsFactura daofactura;

        public FacturaModel()
        {
            daofactura = new implements.DaoImplementsFactura();
        }
        public List<Factura> GetListFactura()
        {
            return daofactura.findAll();
        }

        public void save(DataRow factura)
        {
            Factura f = new Factura();
            f.Id = Convert.ToInt32(factura["Id"].ToString());
            f.Cod_factura = factura["CodFactura"].ToString();
            int empleadoId = int.Parse(factura["Empleado"].ToString());
            f.Empleado = new EmpleadoModel().findById(empleadoId);
            int clienteId = int.Parse(factura["Cliente"].ToString());
            f.Cliente = new ClienteModel().findById(clienteId);
            f.Fecha = DateTime.Parse(factura["Fecha"].ToString());
            f.Observaciones = factura["Observaciones"].ToString();
            f.Subtotal = double.Parse(factura["Subtotal"].ToString());
            f.Iva = double.Parse(factura["Iva"].ToString());
            f.Total = double.Parse(factura["Total"].ToString());

            daofactura.save(f);
        }

        public Factura findById(int id)
        {
            return daofactura.findById(id);
        }
    }
}

