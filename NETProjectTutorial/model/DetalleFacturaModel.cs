﻿using NETProjectTutorial.entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.model
{
    class DetalleFacturaModel
    {
        private implements.DaoImplementsDetalleFactura daodetallefactura;

        public DetalleFacturaModel()
        {
            daodetallefactura = new implements.DaoImplementsDetalleFactura();
        }

        public List<DetalleFactura> GetDetalleFactura()
        {
            return daodetallefactura.findAll();
        }

        public void save(DataRow detalleFactura)
        {
            DetalleFactura df = new DetalleFactura();
            df.Id = Convert.ToInt32(detalleFactura["Id"].ToString());
            int facturaId = int.Parse(detalleFactura["Factura"].ToString());
            df.Factura = new FacturaModel().findById(facturaId);
            int productoId = int.Parse(detalleFactura["Producto"].ToString());
            df.Producto = new ProductoModel().findById(productoId);
            df.Cantidad = int.Parse(detalleFactura["Cantidad"].ToString());
            df.Precio = double.Parse(detalleFactura["Precio"].ToString());

            daodetallefactura.save(df);
        }
    }
}
