﻿using NETProjectTutorial.entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.model
{
    class EmpleadoModel
    {
        private static List<Empleado> ListEmpleados = new List<Empleado>();
        private implements.DaoImplementsEmpleado daoempleado;

        public EmpleadoModel()
        {
            daoempleado = new implements.DaoImplementsEmpleado();
        }
        
        public  List<Empleado> GetListEmpleado()
        {
            return daoempleado.findAll() ;
        }

        public  void Populate()
        {
            /*Empleado[] empleados =
            {
                new Empleado(1, "Pepito", "Pérez", "001-260201-1025V", "1658574-2", "Del arbolito 2c. abajo", 10000, "22528274", "81615721", Empleado.SEXO.MASCULINO),
                new Empleado(2, "Ana", "Conda", "001-260201-1025F", "1785965-2", "Del arbolito 2c. abajo", 50000, "22528274", "83264848", Empleado.SEXO.FEMENINO),
                new Empleado(3, "Juan", "Camaney", "001-260201-1025J", "235689-2", "Del arbolito 2c. abajo", 10000, "22528274", "85987654", Empleado.SEXO.MASCULINO)
            };*/

            ListEmpleados = JsonConvert.DeserializeObject<List<Empleado>>(System.Text.Encoding.Default.GetString(NETProjectTutorial.Properties.Resources.Empleado_data));
            foreach (Empleado e in ListEmpleados)
            {
                daoempleado.save(e);
            }
        }
        public void save(DataRow empleado)
        {
            Empleado e = new Empleado();
            e.Id = Convert.ToInt32(empleado["Id"].ToString());
            e.Nombre = empleado["Nombres"].ToString();
            e.Apellidos = empleado["Apellidos"].ToString();
            e.Cedula = empleado["Cedula"].ToString();
            e.Inss = empleado["Inss"].ToString();
            e.Direccion = empleado["Direccion"].ToString();
            e.Salario = Convert.ToDouble(empleado["Salario"].ToString());
            e.Tconvencional = empleado["telefono"].ToString();
            e.Tcelular = empleado["celular"].ToString();
            e.Sexo = (Empleado.SEXO)Enum.Parse(typeof(Empleado.SEXO),empleado["Sexo"].ToString());
            daoempleado.save(e);

        }
        public void update(DataRow empleado)
        {
            Empleado e = new Empleado();
            e.Id = Convert.ToInt32(empleado["Id"].ToString());
            e.Nombre = empleado["Nombres"].ToString();
            e.Apellidos = empleado["Apellidos"].ToString();
            e.Cedula = empleado["Cedula"].ToString();
            e.Inss = empleado["Inss"].ToString();
            e.Direccion = empleado["Direccion"].ToString();
            e.Salario = Convert.ToDouble(empleado["Salario"].ToString());
            e.Tconvencional = empleado["telefono"].ToString();
            e.Tcelular = empleado["celular"].ToString();
            e.Sexo = (Empleado.SEXO)Enum.Parse(typeof(Empleado.SEXO), empleado["Sexo"].ToString());
            daoempleado.update(e);
        }

        public Empleado findById(int id)
        {
            return daoempleado.findById(id);
        }

       
    }
}
